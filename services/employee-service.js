'use strict';
//Employees records
let employees = [
    {
        id: 1,
        firstName: "Employee",
        lastName: "1",
        title: "CEO",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "amy@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 2,
        firstName: "Employee",
        lastName: "2",
        title: "VP of Engineering",
        managerId: 1,
        managerName: "Amy Taylor",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "anup@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 3,
        firstName: "Employee",
        lastName: "3",
        title: "VP of Marketing",
        managerId: 1,
        managerName: "Amy Taylor",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "michael@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 4,
        firstName: "Employee",
        lastName: "4",
        title: "VP of Sales",
        managerId: 1,
        managerName: "Amy Taylor",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "caroline@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 5,
        firstName: "Employee",
        lastName: "5",
        title: "Account Executive",
        managerId: 4,
        managerName: "Caroline Kingsley",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "james@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 6,
        firstName: "Employee",
        lastName: "6",
        title: "Account Executive",
        managerId: 4,
        managerName: "Caroline Kingsley",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "jen@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 7,
        firstName: "Employee",
        lastName: "7",
        title: "Account Executive",
        managerId: 4,
        managerName: "Caroline Kingsley",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "jonathan@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 8,
        firstName: "Employee",
        lastName: "8",
        title: "Account Executive",
        managerId: 4,
        managerName: "Caroline Kingsley",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "kenneth@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 9,
        firstName: "Employee",
        lastName: "9",
        title: "Software Architect",
        managerId: 2,
        managerName: "Anup Gupta",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "lisa@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 10,
        firstName: "Employee",
        lastName: "10",
        title: "Software Architect",
        managerId: 2,
        managerName: "Anup Gupta",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "brad@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 11,
        firstName: "Employee",
        lastName: "11",
        title: "Software Architect",
        managerId: 2,
        managerName: "Anup Gupta",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "michelle@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 12,
        firstName: "Employee",
        lastName: "12",
        title: "Marketing Manager",
        managerId: 3,
        managerName: "Michael Jones",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "miriam@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 13,
        firstName: "Employee",
        lastName: "13",
        title: "Marketing Manager",
        managerId: 3,
        managerName: "Michael Jones",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "olivia@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 14,
        firstName: "Employee",
        lastName: "14",
        title: "Marketing Manager",
        managerId: 3,
        managerName: "Michael Jones",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "robert@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 15,
        firstName: "Employee",
        lastName: "15",
        title: "Software Architect",
        managerId: 2,
        managerName: "Anup Gupta",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "tammy@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    },
    {
        id: 16,
        firstName: "Employee",
        lastName: "16",
        title: "Account Executive",
        managerId: 4,
        managerName: "Caroline Kingsley",
        phone: "617-123-4567",
        mobilePhone: "617-987-6543",
        email: "victor@fakemail.com",
        picture: "http://172.16.200.34:8558/user-icon-png-person-user-profile-icon-20.png"
    }
];

//Get all records or filter according to the name
exports.findAll = (req, res, next) => {
    let name = req.query.name;
    if (name) {
        let result = employees.filter(employee =>
            (employee.firstName +  ' ' + employee.lastName).toUpperCase().indexOf(name.toUpperCase()) > -1);
        return res.json(result);
    } else {
        return res.json(employees);
    }
};

//Get records according to the id
exports.findById = (req, res, next) => {
    let id = req.params.id;
    let employee = employees[id - 1];
    res.json({
        firstName: employee.firstName,
        lastName: employee.lastName,
        title: employee.title,
        email: employee.email,
        mobilePhone: employee.mobilePhone,
        picture: employee.picture,
        manager: employees[employee.managerId - 1],
        reports: employees.filter(item => item.managerId == id)
    });
}